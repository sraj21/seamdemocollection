//
//  ViewController.swift
//  UICollectionViewDemo
//
//  Created by Raj Upadhyay on 6/19/19.
//  Copyright © 2019 Raj Upadhyay. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtMsg: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var lblNothing: UILabel!
    
    
    var msgArray = NSMutableArray()
    
    var TimestampRound: TimeInterval {
        return (Date().timeIntervalSince1970 * 1000.0).rounded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.transform = CGAffineTransform.init(rotationAngle: -(CGFloat.pi))
        addMSGObserver()
        // Do any additional setup after loading the view.
    }
    
    func addMSGObserver(){
        databaseRoot.observe(.value) { (snapp) in
            print(snapp)
            if ((snapp.value as? NSDictionary)?.allValues as NSArray?) != nil{
                    let unSortArray = ((snapp.value as! NSDictionary).allValues as NSArray)
                    self.msgArray = ((unSortArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "time", ascending: true)]) as NSArray).mutableCopy() as! NSMutableArray
                    self.lblNothing.isHidden = true
                }else{
                    self.msgArray.removeAllObjects()
                    self.lblNothing.isHidden = false
                }
                if self.msgArray.count != 0 && ((self.msgArray[0] as? NSDictionary) == nil) {
                    self.msgArray.removeObject(at: 0)
                }
                self.collectionView.reloadData()
                self.indicator.isHidden = true
                self.collectionView.scrollToItem(at: IndexPath.init(row: self.msgArray.count - 1, section: 0), at: .bottom, animated: true)
        }
    }
    
    @IBAction func btnAddClick(_ sender: UIButton) {
        if !((txtMsg.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: " ")).isEmpty)!){
            sender.isUserInteractionEnabled = false
            let setValue = ["msg":self.txtMsg.text!, "time":String(TimestampRound)]
            databaseRoot.child(String(Int(TimestampRound))).setValue(setValue) { (error, refrence) in
                if (error != nil) {
                    let alert = UIAlertController.init(title: error?.localizedDescription, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    sender.isUserInteractionEnabled = true
                }else{
                    self.txtMsg.text = ""
                    sender.isUserInteractionEnabled = true
                }
                self.indicator.isHidden = true
            }
        }
    }
    
    @IBAction func btnClearFirebaseClick(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "Are you sure want to clear Database?", message: "This action cannot be undone", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Clear", style: .destructive, handler: { (action) in
            databaseRoot.removeValue(completionBlock: { (error, ref) in
                if error != nil{
                    let alert = UIAlertController.init(title: error?.localizedDescription, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    let alert = UIAlertController.init(title: "Database cleared successfully", message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        sender.isUserInteractionEnabled = true
    }
    
    
}
extension ViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return msgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellOne", for: indexPath) as! CollectionViewCellClass
        cell.lblName.text = (msgArray[indexPath.row] as! NSDictionary).value(forKey: "msg") as? String
        cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGFloat = (self.collectionView.frame.width / 2) - 8
        return CGSize(width:size, height: size/2)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }

}

let databaseRoot = Database.database().reference().child("Admin")

class NewCellViewShadow: UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.masksToBounds = false
        backgroundColor = .white
        layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:1).cgColor
        layer.shadowOffset = CGSize.init(width: 0, height: 0)
        layer.shadowRadius = 0.5
        layer.shadowOpacity = 1.0
        layer.cornerRadius = 10
    }
}
